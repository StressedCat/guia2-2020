#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json


# Aca se modificara o se creara la formula
def ModFormula(Formula):
    El = input("Ingrese un elemento de la formula: ")
    Cant = int(input("Ingrese la cantidad: "))
    if El.upper() == "C":
        Formula["C"] = Cant
    elif El.upper() == "H":
        Formula["H"] = Cant
    elif El.upper() == "N":
        Formula["N"] = Cant
    elif El.upper() == "O":
        Formula["O"] = Cant
    elif El.upper() == "S":
        Formula["S"] = Cant
    else:
        print("Dicho elemento no fue encontrado para un aminoacido")


# Para borrar un elemento, se dejara en 0, que es como si no existiera
def Borrar(Formula):
    Del = input("¿Que elemento desea borrar?: \n")
    if Del.upper() == "C":
        Formula["C"] = 0
    elif Del.upper() == "H":
        Formula["H"] = 0
    elif Del.upper() == "N":
        Formula["N"] = 0
    elif Del.upper() == "O":
        Formula["O"] = 0
    elif Del.upper() == "S":
        Formula["S"] = 0
    else:
        print("Dicho elemento no fue encontrado para un aminoacido")


# Para comprobar si existe el aminoacido, se usara la base de datos
def Comprobar(Formula):
    with open('Aminoacidos.json') as f:
        amino = json.load(f)
    for AA in amino['AA']:
        correcto = 0
        if Formula['C'] == AA['C']:
            print("El siguiente aminoacido es parecido en C", AA['Nombre'])
            correcto = correcto + 1
        if Formula['H'] == AA['H']:
            print("El siguiente aminoacido es parecido en H", AA['Nombre'])
            correcto = correcto + 1
        if Formula['N'] == AA['N']:
            print("El siguiente aminoacido es parecido en N", AA['Nombre'])
            correcto = correcto + 1
        if Formula['O'] == AA['O']:
            print("El siguiente aminoacido es parecido en O", AA['Nombre'])
            correcto = correcto + 1
        if Formula['S'] == AA['S']:
            print("El siguiente aminoacido es parecido en S", AA['Nombre'])
            correcto = correcto + 1
        if correcto == 5:
            print("Este aminoacido es: ", AA['Nombre'])
        print("\n")


# Para enviar, se vera si existe el aminoacido creado
def Send(Formula):
    with open('Aminoacidos.json') as f:
        amino = json.load(f)
    send = False
    for AA in amino['AA']:
        correcto = 0
        if Formula['C'] == AA['C']:
            correcto = correcto + 1
        if Formula['H'] == AA['H']:
            correcto = correcto + 1
        if Formula['N'] == AA['N']:
            correcto = correcto + 1
        if Formula['O'] == AA['O']:
            correcto = correcto + 1
        if Formula['S'] == AA['S']:
            correcto = correcto + 1
        if correcto == 5:
            # Si este existe, sera agregado a enviados.json
            print("Este aminoacido es: ", AA['Nombre'])
            nom = AA['Nombre']
            form = AA['form']
            enviados[nom] = form
            with open('enviados.json', 'w') as f:
                json.dump(enviados, f)
            send = True
    if send is False:
        print("No hubo ningun aminoacido similar a su formula")


# El menu, donde se puede seleccionar lo que se quiere
def menu(Formula):
    print("\n")
    print("Su formula creada es la siguiente:")
    if Formula['C'] != 0:
        print("C", Formula['C'])
    if Formula['H'] != 0:
        print("H", Formula['H'])
    if Formula['N'] != 0:
        print("N", Formula['N'])
    if Formula['O'] != 0:
        print("O", Formula['O'])
    if Formula['S'] != 0:
        print("S", Formula['S'])
    print("\n")
    print("Para ingresar o editar la formula, presiona F")
    print("Para borrar la formula, presione D")
    print("Para comprobar que la formula existe, presione C")
    print("Para enviar el aminoacido, presione S")
    print("Para salir, presione X")
    opcion = input("Presione una tecla: ")
    print("\n")
    if opcion.upper() == 'F':
        ModFormula(Formula)
    elif opcion.upper() == 'D':
        Borrar(Formula)
    elif opcion.upper() == 'C':
        Comprobar(Formula)
    elif opcion.upper() == 'S':
        Send(Formula)
    elif opcion.upper() == 'X':
        print("Muchas gracias por su tiempo")
        print("Sus aminoacidos enviados estaran en el archivo enviados.json")
        quit()
    else:
        print("Tecla incorrecta, por favor intente de nuevo")


# El inicio, donde se explica que se hara
if __name__ == "__main__":
    print("Bienvenido al programa de aminoacidos")
    print("Aca podra buscar entre 20 aminoacidos")
    print("Para esto, se le pide porfavor ingresar la formula")
    print("Predeterminadamente, todos los elementos estaran en 0")
    enviados = {}
    Formula = {
        "C": 0,
        "H": 0,
        "N": 0,
        "O": 0,
        "S": 0
    }
    x = 1
    while x != 2:
        menu(Formula)
