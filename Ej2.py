#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json


# Se hara la suma por medio de vivos siendo positivo y muertos negativos
def SumaRecuperadosMuertos(data):
    for Paises in data['Paises']:
        print('Pais: ', Paises['Pais'])
        print("Balance: ", Paises['Recuperados']-Paises['Muertos'])


# Se mostrara todo lo posible del JSON
def MostrarTodo(data):
    for Paises in data['Paises']:
        print('Pais: ', Paises['Pais'])
        print('Confirmados: ', Paises['Confirmados'])
        print('Muertos: ', Paises['Muertos'])
        print('Recuperados: ', Paises['Recuperados'])
        print("\n")


# Se vera que pais esta infectado, junto con sumar todo y decirlo al final
def PaisesContagiados(data):
    Infectados = 0
    for Paises in data['Paises']:
        # Si los confirmados son mayores a 0, esta infectado
        if Paises['Confirmados'] > 0:
            print("Pais: ", Paises['Pais'])
            print("Infectados: ", Paises['Confirmados'])
            Infectados = Infectados+1
    print("Paises infectados en total: ", Infectados)


# Menu donde se seleccionara tarea deseada
def Menu(data):
    print("Ultima actualizacion: 27/03/2020")
    print("Bienvenido a la estadisticas de Corona")
    print("Presione B para obtener el balance entre muertes y recuperados")
    print("Presione S para mostrar toda la lista")
    print("Presione I para mostrar paises infectados")
    print("Presione X para salir del programa")
    elec = input("Ingrese su opcion: ")
    if elec.upper() == 'B':
        SumaRecuperadosMuertos(data)
    elif elec.upper() == 'S':
        MostrarTodo(data)
    elif elec.upper() == 'I':
        PaisesContagiados(data)
    elif elec.upper() == 'X':
        print("Muchas gracias por usar el programa")
        quit()
    else:
        print("Tecla erronea")


if __name__ == "__main__":
    with open('Datos.json') as f:
        data = json.load(f)
        x = 1
        while(x != 2):
            Menu(data)
