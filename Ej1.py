#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Key : Value


# Se añadiran elementos a la formula
def Add(Histidine):
    form = input("¿Cual es la formula? \n")
    amino = input("¿Que aminoacido es? \n")
    Histidine[form.upper()] = amino.title()
    print("\n")


# Se borrara una parte de la formula
def Delete(Histidine):
    for key in Histidine:
        print(key)
    form = input("¿Que formula desea eliminar? \n")
    if form.upper() in Histidine:
        del Histidine[form.upper()]
    else:
        print("No se encontro lo que se intento borrar")
    print("\n")


# Se editara una parte de la formula
def Edit(Histidine):
    print(Histidine)
    form = input("¿Que valor desea editar? \n")
    if form.upper() in Histidine:
        edit = input("¿Por cual valor lo desea cambiar? \n")
        Histidine[form.upper()] = edit.title()
    print("\n")


# Se recordara cual es el objetivo
def Remember():
    print("Usted debe convertir el formato de la forma mas ordenada")
    print(dic)
    print("Por ejemplo, C6 = Histidine")
    print("\n")


# Menu
def menu(Histidine):
    print("Asi se ve su diccionario ahora:")
    print(Histidine)
    print("Para añadir, Presione la tecla A")
    print("Para eliminar, Presione la tecla D")
    print("Para editar, presione la tecla E")
    print("Para recordar que hacer, presione la tecla R")
    print("Para terminar, presione la tecla X")
    opcion = input("Presione una tecla: ")
    print("\n")
    if opcion.upper() == 'A':
        Add(Histidine)
    elif opcion.upper() == 'D':
        Delete(Histidine)
    elif opcion.upper() == 'E':
        Edit(Histidine)
    elif opcion.upper() == 'R':
        Remember()
    elif opcion.upper() == 'X':
        print("Muchas gracias por su tiempo")
        quit()
    else:
        print("Tecla incorrecta, por favor intente de nuevo")
        pass


# El comienzo, donde el menu caera en un ciclo sin fin
if __name__ == "__main__":
    dic = {'Histidine': 'C6H9N3O2'}
    print("Buenas tardes, se necesita ordenar las siguientes formulas")
    print(dic)
    print("Por ejemplo: C6 = Histidine")
    print("Estos seran ordenados en un diccionario nuevo")
    Histidine = {}
    x = 0
    while(x != 2):
        menu(Histidine)
